package cl.smorales.ionix.Fragment;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smorales.ionix.MainActivity;
import cl.smorales.ionix.R;
import cl.smorales.ionix.Utils.DesEncoder;
import cl.smorales.ionix.Utils.RetrofitInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    private static String TAG = MainFragment.class.getSimpleName();

    View view;
    @BindView(R.id.mainUserIdText) TextInputLayout userId;
    @BindView(R.id.mainLoginButton) Button loginButton;


    public static MainFragment newInstace(){
        MainFragment mainFragment  = new MainFragment();
        return mainFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        loginButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainLoginButton:
                sendRequest(DesEncoder.getInstance().encodeString(userId.getEditText().getText().toString()));
                break;
        }
    }

    private void goTolistFragment(JSONArray itemArray){
        ((MainActivity)getActivity()).setListFragment(itemArray);
    }

    private void sendRequest(String encodeRut){
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(RetrofitInterface.URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        RetrofitInterface api = adapter.create(RetrofitInterface.class);

        Call<String> call = api.search(encodeRut);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d(TAG, "onResponse: " + response);
                try {
                    JSONObject resp = new JSONObject(response.body());
                    JSONArray items = resp.getJSONObject("result").getJSONArray("items");
                    goTolistFragment(items);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast toast =
                        Toast.makeText(getContext(),
                                getContext().getString(R.string.userError), Toast.LENGTH_SHORT);

                toast.show();
            }
        });
    }

}
