package cl.smorales.ionix.Utils;

import android.content.Context;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class DesEncoder {


    private static String TAG = DesEncoder.class.getSimpleName();
    private Context context;
    private static DesEncoder instance = null;

    private static final String PRIVATE_KEY = "ionix123456";


    /**
     * Singleton implementation;
     */


    public static DesEncoder getInstance(){
        if (instance == null)
            instance = new DesEncoder();
        return instance;
    }


    public String encodeString(String inputString){
        String response = "";
        try {

            DESKeySpec keySpec = new DESKeySpec(PRIVATE_KEY.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(keySpec);
            byte[] cleartext = inputString.getBytes();
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            response = Base64.encodeToString(cipher.doFinal(cleartext),Base64.DEFAULT);


        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return response;

    }

}
