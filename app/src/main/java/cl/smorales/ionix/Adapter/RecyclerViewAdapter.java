package cl.smorales.ionix.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smorales.ionix.MainActivity;
import cl.smorales.ionix.Model.Item;
import cl.smorales.ionix.R;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {


    private AlertDialog dialog;


    List<Item> itemList;
    Context context;

    public RecyclerViewAdapter(List<Item> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }


    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, final int i) {
        Item item = itemList.get(i);
        viewHolder.name.setText(item.getName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(succesDialog("Email:" + itemList.get(i).getEmail() + "\n" +
                        "Teléfono" + itemList.get(i).getPhoneNumber(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismissDialog();
                    }
                }));
            }
        });
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name) TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void showDialog(final AlertDialog.Builder builder){
        MainActivity activity = ((MainActivity)context.getApplicationContext());
        if (!activity.isFinishing()){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog = builder.create();
                    dialog.show();
                }
            });
        }
    }

    private void dismissDialog(){
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


    private AlertDialog.Builder succesDialog(String message, DialogInterface.OnClickListener listener){
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.ok, listener);
        return builder;
    }

}
