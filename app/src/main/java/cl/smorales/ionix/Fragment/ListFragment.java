package cl.smorales.ionix.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smorales.ionix.Adapter.RecyclerViewAdapter;
import cl.smorales.ionix.Model.Item;
import cl.smorales.ionix.R;

public class ListFragment extends Fragment {

    private static String TAG = ListFragment.class.getSimpleName();

    View view;
    JSONArray itemArray;

    @BindView(R.id.list) RecyclerView itemListView;

    public static ListFragment newInstace(String itemArray){
        ListFragment listFragment  = new ListFragment();
        Bundle args = new Bundle();
        args.putString("itemArray", itemArray);
        listFragment.setArguments(args);
        return listFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        Bundle args = getArguments();
        try {
            itemArray = new JSONArray(args.getString("itemArray"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        List<Item> items;
        Type listType = new TypeToken<List<Item>>() {
        }.getType();
        items= new Gson().fromJson(String.valueOf(itemArray), listType);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items, getContext());
        itemListView.setAdapter(adapter);
        return view;
    }

}
