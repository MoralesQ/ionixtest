package cl.smorales.ionix;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONArray;

import cl.smorales.ionix.Fragment.ListFragment;
import cl.smorales.ionix.Fragment.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setMainFragment();
    }



    private void setMainFragment(){
        MainFragment mainFragment = MainFragment.newInstace();
        setFragment(mainFragment);
    }

    public void setListFragment(JSONArray itemArray){
        ListFragment listFragment =  ListFragment.newInstace(itemArray.toString());
        setFragment(listFragment);
    }


    private void setFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.mainFrameLayout, fragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
    }

}
