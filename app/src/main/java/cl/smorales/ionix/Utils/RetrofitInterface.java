package cl.smorales.ionix.Utils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitInterface {

    String URL = "https://sandbox.ionix.cl/test-tecnico/";

    @GET("test-tecnico/search?rut=")
    Call<String> getString();

    @GET("search")
    Call<String> search(@Query("rut") String rut);
}
